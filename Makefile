all:
	xelatex -halt-on-error LIVRO.tex
	xelatex -halt-on-error LIVRO.tex
test:
	xelatex LIVRO.tex
	xelatex LIVRO.tex
	evince LIVRO.pdf
clean:
	rm *aux *log *tui *toc
